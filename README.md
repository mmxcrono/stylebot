# stylebot

These are css styles to use with Chrome Plugin [Stylebot](https://chrome.google.com/webstore/detail/stylebot/oiaejidbmkiecgbjeifoejpgmdaleoha)

# Instructions for styling a website

1. Use the Click the CSS plugin icon at the top right of Chrome
2. Click on Edit CSS
3. Paste the style in
4. Click Save

